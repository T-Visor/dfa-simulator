package dfa.simulator;

import java.util.*;

/**
 * This program will simulate a Deterministic Finite Automaton (DFA).
 * 
 * We assume the language is in the alphabet {0,1} with a maximum of 20 states.
 * 
 * This program will build a DFA and determine if a given input string is
 * accepted or not.
 * 
 * @author T-Visor
 */
public class DFASimulator 
{
    public static final int TUPLE_SIZE = 3;
    
    public static void main(String[] args)
    {
        String decision;
        boolean playAgain;
        Scanner input = new Scanner(System.in);
       
        // get the transition functions
        ArrayList<int[]> transitionFunctions = getTransitionFunctions(input);
        
        // get the accepting states
        ArrayList<Integer> acceptingStates = getAcceptingStates(input);
        
        // build the hash map
        HashMap<String, String> map = buildMap(transitionFunctions);
        
        // display the DFA transition function table and accepting states
        System.out.println();
        printTransitionFunctions(transitionFunctions);
        printAcceptingStates(acceptingStates);
        
        // declare an iterator variable for the array
        // which will be used to get keys from the hash map
        int iterator = 0;
        String[] keys = new String[map.size()];
        
        // get every key from the hash map
        for (String key : map.keySet()) 
        {
            keys[iterator] = key;
            iterator++;
        }
        
        // run the Deterministic Finite Automaton simulation
        do
        {
            runDFA(input, keys, map, acceptingStates);

            System.out.print("Would you like to check another word (y/n)? ");
            decision = input.next();
            System.out.println();

            if (decision.equals("y"))
                playAgain = true;
            else
                playAgain = false;
        } while (playAgain);

        System.out.println("Finished!");
    }
    
    //========================================================================
    
    /**
     * Continuously prompt the user for the transition states
     * in the Deterministic Finite Automaton (DFA).
     * 
     * @param input Scanner for user input
     * 
     * @return an ArrayList which holds a collection of 
     *         transition functions for the DFA
     */
    public static ArrayList getTransitionFunctions(Scanner input)
    {
        String response;
        boolean keep_looping = true; 
        
        // holds the transition function from one state to the next
        // format: transitionFunction[0] = old state
        //         transitionFunction[1] = symbol
        //         transitionFunction[2] = new state
        int transitionFunction[] = new int[TUPLE_SIZE];
        
        // holds the collection of transition functions
        ArrayList<int[]> transitionFunctions = new ArrayList<>(40);
        
        while (keep_looping)
        {
            System.out.print("Enter the old state, symbol, then new state (on same line with spaces): ");
            int oldState = input.nextInt();
            int symbol = input.nextInt();
            int newState = input.nextInt();
            input.nextLine();
            
            // store the values temporarily into transitionFunction
            transitionFunction[0] = oldState;
            transitionFunction[1] = symbol;
            transitionFunction[2] = newState;
      
            // add an entry to the collection
            int[] addMe = transitionFunction.clone();
            transitionFunctions.add(addMe);
            
            System.out.print("Do you have another state to add (y/n)? ");
            response = input.next();
            input.nextLine();
            System.out.println();
            
            if (response.equals("n"))
                keep_looping = false;
        }
        
        return transitionFunctions;
    }
    
    /**
     * Prompt the user and get a set of accepting states for the Deterministic
     * Finite Automaton (DFA).
     * 
     * @param input the Scanner for user input
     * 
     * @return an ArrayList of Integers holding the accepting states
     */
    public static ArrayList getAcceptingStates(Scanner input)
    {
        ArrayList<Integer> acceptingStates = new ArrayList<>();
        
        System.out.print("Enter a set of accepting states seperated by a space: ");
        String line = input.nextLine();
        
        Scanner lineReader = new Scanner(line);
        int value;
        
        while (lineReader.hasNext())
        {
            value = Integer.parseInt(lineReader.next());
            acceptingStates.add(value);
        }
        
        return acceptingStates;
    }
    
    /**
     * Display the transition states for the Deterministic Finite Automaton (DFA).
     * 
     * Example output:
     *--------------TRANSITION FUNCTION TABLE--------------
     * OLD STATE         SYMBOL		NEW STATE
     * 1                 0 		1 	
     * 
     * @param transitionFunctions the ArrayList holding the transition functions
     */
    public static void printTransitionFunctions(ArrayList<int[]> transitionFunctions)
    {
        System.out.println("\n--------------TRANSITION FUNCTION TABLE--------------");
        System.out.println("OLD STATE\tSYMBOL\t\tNEW STATE");
        
        for (int row = 0; row < transitionFunctions.size(); row++)
        {
            for (int column = 0; column < TUPLE_SIZE; column++)
            {
                System.out.print(transitionFunctions.get(row)[column] + " \t\t");
            }
            System.out.println();
        }
    }
    
    /**
     * Display the accepting states for the Deterministic Finite Automaton (DFA).
     * 
     * Example output: ACCEPTING STATES ==> 1 2 5
     * 
     * @param acceptingStates The ArrayList holding the accepting states
     */
    public static void printAcceptingStates(ArrayList<Integer> acceptingStates)
    {
        System.out.print("ACCEPTING STATES ==> ");
        
        for (int i = 0; i < acceptingStates.size(); i++)
        {
            System.out.print(acceptingStates.get(i) + " ");
        }
        System.out.println("\n\n");
    }
    
    /**
     * Builds a HashMap storing the 3-tuple transition function as the key
     * and the new state as the corresponding string value.
     * 
     * Example: Old state = 1, 
     *          Symbol = 0, 
     *          New state = 2
     * 
     *          Key = "102"    (old state, symbol, new state)
     *          value = "2"    (new state) 
     * 
     * @param transitionFunctions the ArrayList holding the 3-tuple transition functions
     * 
     * @return a HashMap which stores the transition functions and corresponding
     *         new states
     */
    public static HashMap buildMap(ArrayList<int[]> transitionFunctions)
    {
        String key = "";
        HashMap<String, String> map = new HashMap<String, String>();
        
        for (int i = 0; i < transitionFunctions.size(); i++)
        {
            for (int j = 0; j < TUPLE_SIZE; j++)
            {
                key += transitionFunctions.get(i)[j];
            }
            
            // convert the new state to a string
            String value = "" + transitionFunctions.get(i)[2];
            
            // place the values in the hash map
            map.put(key, value);
            
            // make this empty to convert the next transition function
            key = "";
        }
        
        return map;
    }
    
    /**
     * Prompts the user to type in a string value in a given alphabet,
     * the method will determine if the given string value is regular by
     * using a Deterministic Finite Automaton (DFA).
     * 
     * @param input Scanner for the keyboard
     * @param keys the array of keys from the HashMap map
     * @param map a hash map which holds the transition functions
     * @param acceptingStates an ArrayList of accepting states for the DFA
     */
    public static void runDFA(Scanner input, String[] keys, 
                              HashMap<String, String> map, ArrayList<Integer> acceptingStates)
    {
        System.out.print("Enter a string in the alphabet {0,1}: ");
        String word = input.next();
        
        int finalState;
        boolean accepted = false;
        String transition = "1";    /* the first state will always be 1 */
        String newState = "";
        
        for (int i = 0; i < word.length(); i++)
        {
            transition += word.charAt(i);
            
            for (int j = 0; j < keys.length; j++)
            {
                // Find the matching key for the transition tuple being built
                if (keys[j].regionMatches(0, transition, 0, 2))
                {
                    // get the value of the hash with the key found,
                    // it returns the new state which is either 
                    // a 1 or 2 digit number
                    newState = map.get(keys[j]);
                    break;
                }
            }
            
            // transition will now hold the new state... we will either
            // go into this loop again to find another state, 
            // or if this is the last state we are finished
            transition = newState;
        }
        
        // display the final state of the DFA
        finalState = Integer.parseInt(transition);
        System.out.println("Ending state: " + transition);
        
        // check to see if the final state is an accepting state
        for (int i = 0; i < acceptingStates.size(); i++)
        {
            if (acceptingStates.get(i) == finalState)
            {
                accepted = true;
                break;
            }
        }
        
        // display the results
        if (accepted)
            System.out.println(word + " is regular and the DFA accepts!\n");
        else
            System.out.println(word + " is NOT regular and is rejected by the DfA\n");
    }
}